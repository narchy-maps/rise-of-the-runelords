### 3.0.2

#### Book 3
- Hook Moutain
- Skull's Crossing - Grazuul's Domain

### 3.0.1

#### Book 3
- Skull's Crossing - Skull's Watch

## 3.0.0

#### Book 3

- Fort Rannick
- Fort Rannick First
- Fort Rannick Second
- Fort Rannick Underground
- Graul Farm
- Skull's Crossing - Western Caverns

## 2.0.0

#### Book 2
- Foxglove Manor (Ground Level)
- Foxglove Manor (Basement, First, Second)
- Foxglove Catacombs
- Sanatorium
- Seven's Sawmill
- Shadow Clock
- Foxglove Townhouse
- Hambley Farm


## 1.0.7

### Initial Release

#### Book 1
- Glassworks
- Catacombs of Wrath
- Thistletop Ground
- Thistletop (Beneath)
- Thistletop (Further Beneatch)

